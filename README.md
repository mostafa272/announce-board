# Announce-Board
A fast and simple Joomla module to show your Announcements to users in a pretty style.

## Installation
Download mod_announceboard.zip and install it in Joomla. If you download release files, you should extract it to find mod_announceboard.zip file.

## Donate to support us                                                 
                                                                   
[![Donate with Bitcoin](https://en.cryptobadges.io/badge/small/16f1DStB3YG3R4BMTa1zGYRxN9i7FAqtUX)](https://en.cryptobadges.io/donate/16f1DStB3YG3R4BMTa1zGYRxN9i7FAqtUX)
                                                   
  [![Donate with Bitcoin](https://en.cryptobadges.io/badge/big/16f1DStB3YG3R4BMTa1zGYRxN9i7FAqtUX)](https://en.cryptobadges.io/donate/16f1DStB3YG3R4BMTa1zGYRxN9i7FAqtUX)
                                            


